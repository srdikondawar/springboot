package com.test.ui.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.test.orm.beans.TestData;
import com.test.orm.dao.TestDataDAO;
import com.test.util.ResponseModel;

@RestController
@RequestMapping(path = "/testdata")
@SuppressWarnings("unchecked")

public class TestDataController {

	/**
	 * Injecting dao into controller
	 */
	@Autowired
	private TestDataDAO testDataDAO;

//	/**
//	 * This api will Update the TestData
//	 * 
//	 * @param TestData
//	 * @return
//	 */
//	@RequestMapping(value = "/mod", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	public @ResponseBody ResponseModel modTestData(@RequestBody TestData testData) {
//
//		ResponseModel responseModel = new ResponseModel();
//		try {
//
//			if (testData != null && testData.getId() != null) {
//
//				TestData bean = testDataDAO.findById(testData.getId());
//				if (bean != null) {
//
//					bean.setAge(testData.getAge());
//					bean.setName(testData.getName());
//					testDataDAO.merge(bean);
//
//					responseModel.setStatusCode(1);
//					responseModel.setStatusDesc("Success");
//				} else {
//					throw new Exception();
//				}
//			} else {
//				throw new Exception();
//			}
//
//		} catch (Exception e) {
//			responseModel.setStatusCode(0);
//			responseModel.setStatusDesc("Failed");
//
//		}
//		return responseModel;
//	}
//
//	/**
//	 * This api will Update the TestData
//	 * 
//	 * @param TestData
//	 * @return
//	 */
//	@RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//	public @ResponseBody ResponseModel addTestData(@RequestBody TestData testData) {
//
//		ResponseModel responseModel = new ResponseModel();
//		try {
//
//			TestData bean = new TestData();
//			bean.setId(testData.getId());
//			bean.setAge(testData.getAge());
//			bean.setName(testData.getName());
//
//			testDataDAO.save(bean);
//
//			responseModel.setStatusCode(1);
//			responseModel.setStatusDesc("Success");
//
//		} catch (Exception e) {
//			responseModel.setStatusCode(0);
//			responseModel.setStatusDesc("Failed");
//
//		}
//		return responseModel;
//	}
//
//	/**
//	 * Delete Test data by Id
//	 * 
//	 * @param id
//	 * @return
//	 */
//	@RequestMapping(value = "/delete/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	public @ResponseBody ResponseModel removeTestDataById(@PathVariable Long id) {
//		ResponseModel responseModel = new ResponseModel();
//		try {
//			testDataDAO.delete(testDataDAO.findById(id));
//			responseModel.setStatusCode(1);
//			responseModel.setStatusDesc("Success");
//		} catch (Exception e) {
//			responseModel.setStatusCode(0);
//			responseModel.setStatusDesc("Failed");
//
//		}
//		return responseModel;
//
//	}

	/**
	 * Fetching all Test Data
	 * 
	 * @return
	 */
	@RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<TestData> getAllTestData() {
		return testDataDAO.findAll();
	}

}
