package com.test.orm.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.test.orm.beans.TestData;

 
public interface TestDataDAO  extends  JpaRepository<TestData,Long>{
	
	List<TestData> findByAge(String age);

	 

}
